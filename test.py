import random
import time
# import random is used to randomly choose an item from a list [] or sequence
# import time is used to import actual time from your pc to use in the program


# Initial Steps to invite user to play game

print ("\nWelcome to Hangman brought to you by Arshia Rahman\n")
name = input("Enter your name: ")
print ("Hello " + name + "! Best of Luck!")
# time.sleep suspends execution for the given number of seconds. Argument may be a floating number.
time.sleep (2)
print ("The game is about to start! \n Lets play Hangman!")
time.sleep (3)

# Define main functions

def main ():
    global count
    global display
    global word
    global already_guessed
    global length
    global play_game

words_to_guess =
["Magic","President","Mountains", "Europe", "Destiny", "Weather", "Chalkboard", "Promise", "Wisdom", "Heart", "Solar System"]
word = random.choice (words_to_guess)
length = len(word)
count = 0
display = '_' * length
already_guessed = []
play_game = ""

# Loop to re-execute the game after the first round ends:

     def play_loop():
    global play_game
    play_game = input ("Do you want to play again? y = yes, n = no \n")
    while play_game not in [ "y", "n","Y","N"]:
        play_game = input ("Do you want to play again? y = yes, n = no \n")
        if play_game == "yes":
            main()
        elif play_game == "no":
+#Initialize all the conditions required for the game
        def hangman () :
            global count
            global display
            global word
            global already_guessed
            global play_game
            limit = 5
            guess = input("This is the hangman word: " + display + "Enter your guess: \n")
            guess = guess.strip()
            if len(guess.strip()) == 0 or len (guess.strip()) >= 2 or gues <= "9":
                print("Invalid Input, Try a letter \n")
                hangman()

            elif guess in word:
                already_guessed.extend([guess])
                index = word.find (guess)
                word = word [:index] + "_" + word[index + 1:]
                display = display[:index]
                print(display + "\n")

            elif guess in already_guessed:
                print("Try another letter.\n")

            else:
                count += 1

                if count == 1:
                    time.sleep(1)
                    print("")
                    print("   _____ \n"
                          "   |      \n"
                          "   |       \n"
                          "   |        \n"            
                          "   |         \n"
                          "   |          \n"